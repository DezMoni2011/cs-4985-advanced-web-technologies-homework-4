﻿
public partial class Cart : System.Web.UI.Page
{
    private CartItemList _cart;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this._cart = CartItemList.GetCart();

        if (!IsPostBack)
        {
            this.DisplayCart();
        }
    }

    protected void btnRemove_Click(object sender, System.EventArgs e)
    {
        if (this._cart.Count <= 0)
        {
            return;
        }
        if (this.lstCart.SelectedIndex > -1)
        {
            this._cart.RemoveAt(this.lstCart.SelectedIndex);
            this.DisplayCart();
        }
        else
        {
            this.lblMessage.Text = "Please select an item to remove";
        }
    }

    private void DisplayCart()
    {
        this.lstCart.Items.Clear();

        for (var i = 0; i < this._cart.Count; i++)
        {
            this.lstCart.Items.Add(this._cart[i].Display());
        }
    }


    protected void btnEmpty_Click(object sender, System.EventArgs e)
    {
        if (this._cart.Count <= 0)
        {
            return;
        }
        this._cart.Clear();
        this.lstCart.Items.Clear();
    }
    protected void btnCheckOut_Click(object sender, System.EventArgs e)
    {
        this.lblMessage.Text = "Sorry, that function hasn't been implemented yet.";
    }
}