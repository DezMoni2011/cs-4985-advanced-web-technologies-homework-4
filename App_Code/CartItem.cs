using System.Diagnostics;

/// <summary>
/// This class represents an item from the CartItemsList to be displayed in Murach textbook's Halloween Application. (the code has been modified)
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// version: January 20, 105 | Spring
/// </version>
public class CartItem
{
    /// <summary>
    /// The product
    /// </summary>
    private Product _aProduct;
    /// <summary>
    /// The quantity
    /// </summary>
    private int _aQuantity;

    /// <summary>
    /// Initializes a new instance of the <see cref="CartItem" /> class.
    /// </summary>
    public CartItem() {}

    /// <summary>
    /// Initializes a new instance of the <see cref="CartItem" /> class.
    /// </summary>
    /// <param name="product">The product.</param>
    /// <param name="quantity">The quantity.</param>
    public CartItem(Product product, int quantity)
    {
        this.Product = product;
        this.Quantity = quantity;
    }

    /// <summary>
    /// Gets or sets the product.
    /// </summary>
    /// <value>
    /// The product.
    /// </value>
    public Product Product
    {
        get
        {
            return this._aProduct;
        }
        set
        {
            Trace.Assert(value != null, "Invalid Product");
            this._aProduct = value;
        }
    }
    /// <summary>
    /// Gets or sets the quantity.
    /// </summary>
    /// <value>
    /// The quantity.
    /// </value>
    public int Quantity 
    {
        get
        {
            return this._aQuantity;
            
        }
        set
        {
            Trace.Assert(value < 0, "Invalid Quantity");
            this._aQuantity = value;
        } 
    }

    /// <summary>
    /// Adds the quantity.
    /// </summary>
    /// <param name="quantity">The quantity.</param>
    public void AddQuantity(int quantity)
    {
        this.Quantity += quantity;
    }

    /// <summary>
    /// Displays this instance.
    /// </summary>
    /// <returns>
    /// String representation the object.
    /// </returns>
    public string Display()
    {
        var displayString =
            this.Product.Name + " (" + this.Quantity.ToString()
            + " at " + this.Product.UnitPrice.ToString("c") + " each)";

         return displayString;
    }
    
}