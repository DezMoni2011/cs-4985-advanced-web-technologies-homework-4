using System.Diagnostics;

/// <summary>
/// This class represenets a Product to be used in Murach Textbook for the Halloween Application.
/// </summary>
/// <author>Destiny Harris</author>
/// <version>
/// January 20, 2015 | Spring
/// </version>
public class Product
{
    /// <summary>
    /// The product identifier
    /// </summary>
    private string _productId;
    /// <summary>
    /// The name
    /// </summary>
    private string _name;
    /// <summary>
    /// The short description
    /// </summary>
    private string _shortDescription;
    /// <summary>
    /// The long description
    /// </summary>
    private string _longDescription;
    /// <summary>
    /// The unit price
    /// </summary>
    private decimal _unitPrice;
    /// <summary>
    /// The image file
    /// </summary>
    private string _imageFile;

    /// <summary>
    /// Gets or sets the product identifier.
    /// </summary>
    /// <value>
    /// The product identifier.
    /// </value>
    public string ProductId
    {
        get { return this._productId; }
        set
        {
            Trace.Assert(value != null, "Invalid Product ID.");
            this._productId = value;
        }
    }

    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    /// <value>
    /// The name.
    /// </value>
    public string Name
    {
        get { return this._name; }
        set
        {
            Trace.Assert(value != null, "Invalid name.");
            this._name = value;
        }
    }

    /// <summary>
    /// Gets or sets the short description.
    /// </summary>
    /// <value>
    /// The short description.
    /// </value>
    public string ShortDescription
    {
        get { return this._shortDescription; }
        set
        {
            Trace.Assert(value != null, "Invalid short description.");
            this._shortDescription = value;
        }
    }

    /// <summary>
    /// Gets or sets the long description.
    /// </summary>
    /// <value>
    /// The long description.
    /// </value>
    public string LongDescription
    {
        get
        {
            return this._longDescription''
        }
        set
        {
            Trace.Assert(value != null, "Invalid long description.");
            this._longDescription = value;
        }
    }

    /// <summary>
    /// Gets or sets the unit price.
    /// </summary>
    /// <value>
    /// The unit price.
    /// </value>
    public decimal UnitPrice
    {
        get { return this._unitPrice; }
        set
        {
            Trace.Assert(value < 0, "Invalid unit price.");
            this._unitPrice = value;
        }
    }

    /// <summary>
    /// Gets or sets the image file.
    /// </summary>
    /// <value>
    /// The image file.
    /// </value>
    public string ImageFile
    {
        get { return this._imageFile; }
        set
        {
            Trace.Assert(value != null, "Invalid image file.");
            this._imageFile = value;
        }
    }
}