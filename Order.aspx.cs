﻿
using System;
using System.Data;
using System.Web.UI;

public partial class Order : Page
{
    private Product _selectedProduct;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) this.ddlProducts.DataBind();
        this._selectedProduct = this.GetSelectedProduct();
        this.lblName.Text = this._selectedProduct.Name;
        this.lblShortDescription.Text = this._selectedProduct.ShortDescription;
        this.lblLongDescription.Text = this._selectedProduct.LongDescription;
        this.lblUnitPrice.Text = this._selectedProduct.UnitPrice.ToString("c") + " each ";
        this.imgProduct.ImageUrl = "Images/Products/" + this._selectedProduct.ImageFile;
    }

    private Product GetSelectedProduct()
    {
        var productsTable = (DataView) this.SqlDataSource1.Select(DataSourceSelectArguments.Empty);
        var p = new Product();

        if (productsTable == null)
        {
            return p;
        }
        productsTable.RowFilter = string.Format("ProductID = '{0}'", this.ddlProducts.SelectedValue);
        var row = productsTable[0];

            
        p.ProductId = row["ProductID"].ToString();
        p.Name = row["Name"].ToString();
        p.ShortDescription = row["ShortDescription"].ToString();
        p.LongDescription = row["LongDescription"].ToString();
        p.UnitPrice = (decimal)row["UnitPrice"];
        p.ImageFile = row["ImageFile"].ToString();

        return p;
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }
        var cart = CartItemList.GetCart();
        var cartItem = cart[this._selectedProduct.ProductId];

        if (cartItem == null)
        {
            cart.AddItem(this._selectedProduct, Convert.ToInt32(this.txtQuantity.Text));
        }
        else
        {
            cartItem.AddQuantity(Convert.ToInt32(this.txtQuantity.Text));
        }
        Response.Redirect("Cart.aspx");
    }
}
